import {GlobalStyle} from './styles/GlobalStyle'
import {useState} from "react";
import {RJSFSchema} from "@rjsf/utils";
import validator from "@rjsf/validator-ajv8";
import Form from "@rjsf/antd";
import {schema} from './schema/ResourceDescription'

import {Layout, Menu, theme, Button} from 'antd';

const {Header, Content, Footer, Sider} = Layout;


export function App() {
    const [data, setData] = useState({resource: {}});
    const uiSchema: RJSFSchema = {
        "ui:title": "Resource",
        "ui:description": "The rd description of the resource you want to create",
        "ui:autocomplete": "on",
        "ui:submitButtonOptions": {
            "norender": true
        }
    }

    const saveFile = function () {
        window.Main.saveFile(data)
    }
    const openFile = () => window.Main.openFile().then(result => {
        setData(result)
    })
    return (
        <>
            <Layout>
                <Header>
                    <Menu mode="horizontal"
                          theme="dark"
                          items={
                              [
                                  {key: 0, label: "Open", onClick: openFile},
                                  {key: 1, label: "Save", onClick: saveFile}
                              ]
                          }
                    />

                </Header>
                <Content style={{margin: "24px 16px 0"}}>
                    <Form schema={schema}
                          validator={validator}
                          formData={data}
                          uiSchema={uiSchema}
                          onChange={(e) => {
                              setData(e.formData)
                          }}
                          onError={(e) => console.log("errors", e)}></Form>
                    <GlobalStyle/>


                </Content>
            </Layout>
        </>
    )
}