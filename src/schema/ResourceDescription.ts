import {RJSFSchema} from "@rjsf/utils";

export const schema: RJSFSchema = {
    title: "RD file",
    type: "object",
    properties: {
        resource: {
            type: 'object',
            required: ['schema'],
            title: "resource",
            properties: {
                schema: {type: "string", title: "schema", description: "Name of the schema"},
                meta: {
                    type: "array",
                    title: "meta",
                    items: {
                        type: "object",
                        required: ['name'],
                        properties: {
                            name: {type: "string", enum: ['creationData', 'title', 'description'], 'title': "name"},
                            value: {type: "string", title: "value"}
                        }
                    }
                },
                table: {
                    type: "array",
                    title: "table",
                    items: {
                        type: "object",
                        required: ['id'],
                        properties: {
                            id: {type: "string", title: "name"},
                            onDisk: {type: "string", enum: ["True", "False"]},
                            mixin: {type: "string", enum: ["//scs#q3cindex"]},
                            adql: {type: "string", enum: ["True", "False"]},

                            column: {
                                type: "array",
                                title: "column",
                                items: {
                                    type: "object",
                                    required: ['name', 'type'],
                                    properties: {
                                        name: {type: "string"},
                                        type: {
                                            type: "string",
                                            default: "real",
                                            enum: ["smallint", "integer", "bigint", "real", "boolean",
                                                "double precision", "text", "char", "unicode", "date",
                                                "timestamp", "time", "spoint", "scircle", "spoly", "sbox",
                                                "smoc", "bytea", "raw", "file", "box", "vexpr-mjd", "vexpr-string",
                                                "vexpr-float", "vexpr-date", "pql-string", "pql-float", "pql-int",
                                                "pql-date", "pql-upload", "int4range"]
                                        },
                                        description: {type: "string"},
                                        ucd: {type: "string", title: "UCD"},
                                        tablehead: {type: "string"},
                                        verbLevel: {type: "number", title: "Verbosity level"},
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }
}

export default schema;