import {useState} from "react";

interface ColumnType {
    name: string;
    type: string;
}

function appendNewColumn(columns: ColumnType[]): ColumnType[] {
    return [...columns, {
        'name': 'new',
        'type': 'undefined'
    }]
}

const Column = (item: any, itemNumber: number) => {
    const [display, setDisplay] = useState<boolean>(false)
    const toggle = () => {
        setDisplay(!display)
    }
    return (
        <li className="row mt-2">
            <div className="h5 col-2 input-group">
                <span className="input-group-text">Name</span>
                <input type="text" className="form-control" placeholder={item.name}/>
                <button className="btn btn-primary pe-auto" style={{width: '2rem'}}
                        onClick={toggle}>{display ? '-' : '+'}</button>

            </div>
            <div className="card m-2 p-2" style={{"display": display ? "" : "none"}}>
                <div className="col">
                    <input type="text" placeholder="Description" className="form-control"/>
                    <div className="form-check form-switch">
                        <label className="form-check-label"
                               htmlFor="hidden">Hidden</label>
                        <input type="checkbox" id="hidden"
                               className="form-check-input"/>
                    </div>


                </div>
            </div>
        </li>
    )
}
export const DynamicTable = () => {
    const [columns, setColumns] = useState<ColumnType[]>([])


    const rows: any[] = columns.map(
        (item: ColumnType, itemNumber: number) => <Column item={item} itemNumber={itemNumber} key={itemNumber
        } name="col"/>
    )
    return (
        <div className="container mt-2">
            <div className="row">
                <input type="text" placeholder="Table name" className="h5 p-1 col form-control"/>
                <button className="btn btn-primary col-2 p-0 ms-4"
                        onClick={() => setColumns(appendNewColumn(columns))}>Add column
                </button>
            </div>
            <ul className="container">
                {rows}
            </ul>

        </div>
    )
}