import {dialog, IpcMainInvokeEvent} from "electron";

import {create, convert} from 'xmlbuilder2'
const fs = require('fs')
function isObject(item: any): boolean {
    return typeof item === 'object' && !Array.isArray(item) && item !== null
}

export function convertToXML(data: any, root: any | null = null, tagName: any | null = null) {
    if (root === null) {
        root = create()
    }

    if (isObject(data)) {

        const elementName = tagName
        const element = elementName !== null ? root.ele(elementName) : root

        for (const [key, value] of Object.entries(data)) {
            if (isObject(value)) {
                convertToXML(value, element, key)
            } else if (Array.isArray(value)) {
                for(const item of value){
                    convertToXML(item, element, key)
                }
            } else if (key === 'value'){
                element.txt(value)
            } else {
                element.att(key, value)
            }
        }
    }
    return root
}

function reshapeXMLObject(xmlObj: any){
    const outObj: any = {}
    for (const [key, value] of Object.entries(xmlObj)) {
        if (key.startsWith('@')){
            outObj[key.replace('@', '')] = value
        } else if (isObject(value)) {
            outObj[key] = reshapeXMLObject(value)
        } else if (Array.isArray(value)){
            outObj[key] = []
            for (const item of value) {
                outObj[key].push(reshapeXMLObject(item))
            }
        } else if (key.startsWith('#')) {
            outObj.value = value
        }
    }
    return outObj
}

export function convertFromXML(xmlString: string) {
    const obj = convert(xmlString, {format: 'object'})

    return reshapeXMLObject(obj)
}


export async function handleSaveToFile(event: IpcMainInvokeEvent, data: any) {
    const {filePath} = await dialog.showSaveDialog(
        {
            title: "Save resource description",
            defaultPath: "q.rd",
            filters: [
                {
                    'name': 'gavo/DACHS resource description',
                    'extensions': ['rd']
                },
                {
                    'name': 'Standard XML',
                    'extensions': ['xml']
                },
            ],
            properties: [
                'createDirectory',
                'showOverwriteConfirmation'
            ]
        }
    )

    if (filePath === undefined) return
    const xmlFile = convertToXML(data).end({prettyPrint: true}).replace('<?xml version="1.0"?>', '')

    fs.writeFile(filePath, xmlFile, (err:any) => {
        if(err){
            console.error(err)
        }
    })
    return 1
}


export async function handleOpenFile(event: IpcMainInvokeEvent) {
    const {canceled, filePaths} = await dialog.showOpenDialog(
        {

            title: "Open resource description",
            defaultPath: "q.rd",
            filters: [
                {
                    'name': 'gavo/DACHS resource description',
                    'extensions': ['rd']
                },
                {
                    'name': 'Standard XML',
                    'extensions': ['xml']
                },
            ],
            properties: [
                'openFile'
            ]
        }
    )

    if (canceled) return
    const content = fs.readFileSync(filePaths[0], "utf-8")

    return convertFromXML(content)

}
