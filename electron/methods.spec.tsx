import {convertToXML, convertFromXML} from './methods'

test('Simple json should be converted into rd', () => {
        const testCase = {
            meta: {'name': 'status', 'description': 'desc'}
        }


        expect(convertToXML(testCase).end({prettyPrint: true})).toStrictEqual(
            "<?xml version=\"1.0\"?>\n" +
            "<meta name=\"status\" description=\"desc\"/>"
        )
    }
);

test('Nested array json should be converted into rd', () => {
    const testCase = {
        meta: {
            'name': 'status', 'description': 'desc', 'meta': [
                {'name': 'status', 'description': 'desc'}
            ]
        }
    }
    expect((convertToXML(testCase).end())).toStrictEqual(
        '<?xml version="1.0"?>' +
        '<meta name="status" description="desc">' +
        '<meta name="status" description="desc"/>' +
        '</meta>'
    )
});

test('Nested json should be converted into rd', () => {
    const testCase = {
        meta: {
            'name': 'status',
            'description': 'desc',
            'meta': {'name': 'status', 'description': 'desc'}
        }
    }
    expect((convertToXML(testCase).end())).toStrictEqual(
        '<?xml version="1.0"?>' +
        '<meta name="status" description="desc">' +
        '<meta name="status" description="desc"/>' +
        '</meta>'
    )
});


test('A more representative example to convert into rd', () => {
    const testCase = {
        resource: {
            meta: [
                {name: 'creationData', value: 'asd'},
                {name: 'title', value: 'asd'}],
            schema: "sasd"
        }
    }
    expect((convertToXML(testCase).end())).toStrictEqual(
        '<?xml version="1.0"?>' +
        '<resource schema="sasd">' +
        '<meta name="creationData">asd</meta>' +
        '<meta name="title">asd</meta>' +
        '</resource>'
    )
})


test('Converts rd into json', () => {
    const testCase = '<?xml version="1.0"?>' +
        '<resource schema="sasd">' +
        '<meta name="creationData">asd</meta>' +
        '<meta name="title">asd</meta>' +
        '</resource>'

    expect((convertFromXML(testCase))).toStrictEqual(
        {
            resource: {
                meta: [
                    {name: 'creationData', value: 'asd'},
                    {name: 'title', value: 'asd'}],
                schema: "sasd"
            }
        }
    )
})
