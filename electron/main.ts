import {app, BrowserWindow, ipcMain} from 'electron'
import {handleSaveToFile, handleOpenFile} from './methods'
let mainWindow: BrowserWindow | null
let devTools: BrowserWindow | null
declare const MAIN_WINDOW_WEBPACK_ENTRY: string
declare const MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY: string

// const assetsPath =
//   process.env.NODE_ENV === 'production'
//     ? process.resourcesPath
//     : app.getAppPath()

function createWindow() {
    mainWindow = new BrowserWindow({
        // icon: path.join(assetsPath, 'assets', 'icon.png'),
        width: 1100,
        height: 900,
        backgroundColor: '#191622',
        webPreferences: {
            nodeIntegration: false,
            contextIsolation: true,
            preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY
        }
    })


    mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY)

    mainWindow.on('closed', () => {
        mainWindow = null
    })

    devTools = new BrowserWindow()

    mainWindow.webContents.setDevToolsWebContents(devTools.webContents)
    mainWindow.webContents.openDevTools({mode: 'detach'})
}



async function registerListeners() {
    /**
     * This comes from bridge integration, check bridge.ts
     */
    ipcMain.on('message', (_, message) => {
        console.log(message)
    })
    ipcMain.handle('save-file', (e, ...args) => handleSaveToFile(e, args[0]))
    ipcMain.handle('open-file', (e, ...args) => handleOpenFile(e))
}

app.on('ready', createWindow)
    .whenReady()
    .then(registerListeners)
    .catch(e => console.error(e))

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})
