import { contextBridge, ipcRenderer } from 'electron'

export const api = {
  /**
   * Here you can expose functions to the renderer process
   * so they can interact with the main (electron) side
   * without security problems.
   *
   * The function below can access using `window.Main.sendMessage`
   */

  sendMessage: (message: string) => {
    ipcRenderer.send('message', message)
  },
  /**
   *
   * The function below can access using `window.Main.saveFile`
   * */
  saveFile: (data: any) => {
    ipcRenderer.invoke('save-file', data).catch(e => console.error(e))
  },
  /**
   *
   * The function below can access using `window.Main.openFile`
   * */
  openFile: (): Promise<any> => {
    return ipcRenderer.invoke('open-file').catch(e => console.error(e))
  },

  /**
   * Provide an easier way to listen to events
   */
  on: (channel: string, callback: Function) => {
    ipcRenderer.on(channel, (_, data) => callback(data))
  }
}

contextBridge.exposeInMainWorld('Main', api)
